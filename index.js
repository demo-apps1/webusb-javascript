
var DEVICE = {
    isConnected: false,
    devices: {},
}

var button = document.getElementById('request-device');

button.addEventListener('click', async function() {
  var device;
  try {
    device = await navigator.usb.requestDevice({ 
        filters: []
    });
  } catch (err) {
    showResult(err);
  }

  if (device !== undefined) {
    DEVICE.isConnected = true;
    DEVICE.devices[device.vendorId] = device;
    
    showResult(device.productName + ' with Serial No: ' + device.serialNumber + ' connected');
  }
});

document.addEventListener('DOMContentLoaded', async function() {
    var devices = await navigator.usb.getDevices();
    devices.forEach(function(device) {
        DEVICE.devices[device.vendorId] = device;
        showResult(device.productName + ' with Serial No: ' + device.serialNumber + ' connected');
    });
});

navigator.usb.addEventListener('connect', event => {
    DEVICE.devices[event.device.vendorId] = event.device;
    showResult(event.device.productName + ' with Serial No: ' + event.device.serialNumber + ' connected');
});

navigator.usb.addEventListener('disconnect', event => {
    delete DEVICE.devices[event.device.vendorId];
    showResult(event.device.productName + ' with Serial No: ' + event.device.serialNumber + ' disconnected');
});

function showResult(data) {
    var date = new Date();
    var dateString = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    var node = document.createElement("li");                 // Create a <li> node
    var textnode = document.createTextNode('['+dateString+'] '+data);         // Create a text node
    node.appendChild(textnode);  
    document.getElementById('device-count').innerHTML = Object.keys(DEVICE.devices).length;
    document.getElementById('result').prepend(node);
}